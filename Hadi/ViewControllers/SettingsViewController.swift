//
//  SettingsViewController.swift
//  HADI and MobileSilencer
//
//  Created by Betim S on 12/24/17.
//  Copyright © 2018 Spectratronix FZCO. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class SettingsViewController: UITableViewController,UITextFieldDelegate {

    @IBOutlet weak var doNotDisturbTextField: JVFloatLabeledTextField!
    let doNotDisturbDefaultTime = 20 //mins
    let kDoNotDisturb = "doNotDisturbTime"
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackBarButton()
        doNotDisturbTextField.delegate = self
        setupDefaults()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        setDoNotDisturbValue(value:textField.text!)
    }
    func setDoNotDisturbValue(value:Any){
        UserDefaults.standard.set(value, forKey: kDoNotDisturb)
        UserDefaults.standard.synchronize()
    }
    func setupDefaults(){
        let returnValue: Double = UserDefaults.standard.double(forKey: kDoNotDisturb)
        if returnValue != 0 {
            doNotDisturbTextField.text = String(describing: returnValue)
        }
        else{
            doNotDisturbTextField.text = String(describing:doNotDisturbDefaultTime)
            setDoNotDisturbValue(value:doNotDisturbDefaultTime)
        }
    }
    func setBackBarButton(){
        let attributes = [NSAttributedStringKey.font:UIFont.fontAwesome(ofSize: 25)]
        let backItem = UIBarButtonItem(title: String.fontAwesomeIcon(name: .chevronRight), style: .done, target: self, action:  #selector(goBack))
        backItem.setTitleTextAttributes(attributes, for: .normal)
        backItem.setTitleTextAttributes(attributes, for: .selected)
        backItem.tintColor = UIColor.white
        self.navigationItem.setRightBarButton(backItem, animated: true)
    }
    @IBAction func goBack(_ sender: Any) {
        self.revealViewController().revealToggle(animated: true)
    }
}
