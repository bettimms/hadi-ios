//
//  ViewController+Estimote.swift
//  HADI and MobileSilencer
//
//  Created by Betim S on 12/27/17.
//  Copyright © 2017 Spectratronix. All rights reserved.
//

import UIKit

extension HomeViewController:ESTBeaconManagerDelegate{    
    
    func setupBeaconMonitoring(){
        self.beaconManager.delegate = self
        self.beaconManager.requestAlwaysAuthorization()
        
        self.beaconManager.startMonitoring(for: CLBeaconRegion(
            proximityUUID: NSUUID(uuidString: "B997FA59-A7FF-4FAD-B415-66E711BFE67E")! as UUID, identifier: "Silent Zone"))
    }
   
    func beaconManager(_ manager: Any, didEnter region: CLBeaconRegion) {
         onEnterTimer()
    }
    func beaconManager(manager: AnyObject, didExitRegion region: CLBeaconRegion) {
        print("didExit range of beacon \(region)")
        showNotification(title: "Quite zone!", body: "You left the quite zone.")
    }
}
