//
//  ViewController+Timers.swift
//  HADI and MobileSilencer
//
//  Created by Betim S on 12/27/17.
//  Copyright © 2017 Spectratronix. All rights reserved.
//

import Foundation
extension HomeViewController{
    @objc func timerAExpired(){
        timerAIsOn = false
        print("timerA has Expired")
        if !timerBIsOn{
            print("timerB expired then timerA expired last exit")
        }
    }
    @objc func timerBExpired(){
        timerBIsOn = false
        print("timerB has Expired")
        if !timerAIsOn {
            print("timerA expired then timerB expired last exit")
        }
    }
    
    func onEnterTimer(){
        //        showNotification(title: "Hello world", body: "Looks like you're near a beacon.")
        if (!timerAIsOn && !timerBIsOn)
        {
            showNotification(title: "Quite zone!",body: "You are entering a Quiet Zone, Please Switch off your Mobile or turn on vibration mode")
            timerAIsOn = true
            print("Timer A will start")
            let doNotDisturbValue: Double = UserDefaults.standard.double(forKey: self.kDoNotDisturb)
            timerA = Timer.scheduledTimer( timeInterval: doNotDisturbValue*60, target: self, selector: #selector(timerAExpired), userInfo: nil, repeats: false)
            
        }
        timerBIsOn = true
        print("Timer B will start")
        timerB = Timer.scheduledTimer( timeInterval: 180  , target: self, selector: #selector(timerBExpired), userInfo: nil, repeats: false)
    }
    
    func setTimer(){
        let returnValue: Double? = UserDefaults.standard.double(forKey: self.kDoNotDisturb)
        if let valueInMinutes  = returnValue{
            let valueInSeconds = valueInMinutes*60
            timerA = Timer.scheduledTimer( timeInterval: valueInSeconds , target: self, selector: #selector(timerAExpired), userInfo: nil, repeats: false)
        }
    }
}
