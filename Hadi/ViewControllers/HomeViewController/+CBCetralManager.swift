//
//  HomeViewController+CBCetralManager.swift
//  HADI and MobileSilencer
//
//  Created by Betim S on 12/27/17.
//  Copyright © 2017 Spectratronix. All rights reserved.
//

import UIKit

extension HomeViewController:CBCentralManagerDelegate{
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
//            UIView.animate(withDuration: 1.5, animations: {
//                self.enableBluetoothView.alpha = 0
//            })
            bluetoothStatusLabel.text = "Bloutooth ON"
            bluetoothStatusLabel.alpha = 0
            isBluetoothOn = true
            break
        default:
//            UIView.animate(withDuration: 1.5, animations: {
//                self.enableBluetoothView.alpha = 1
//            })
            bluetoothStatusLabel.text = "Bloutooth OFF"
            bluetoothStatusLabel.alpha = 1
            isBluetoothOn = false
        }
        checkHadiStatus()
    }
}
