//
//  ViewController.swift
//  HADI and MobileSilencer
//
//  Created by Betim S on 12/15/17.
//  Copyright © 2017 Spectratronix. All rights reserved.
//

import UIKit
import UserNotifications
import SWRevealViewController

class HomeViewController: UIViewController,SWRevealViewControllerDelegate {
    var isSettingsView:Bool = false
    let kDoNotDisturb = "doNotDisturbTime"
    
//    @IBOutlet weak var enableBluetoothView: UIView!
//    @IBOutlet weak var enableLocationView: UIView!
    let center = UNUserNotificationCenter.current()
    
     let locationManager = CLLocationManager()
    
    @IBOutlet weak var hadiStatusLabel: UILabel!
    @IBOutlet weak var bluetoothStatusLabel: UILabel!
    @IBOutlet weak var locationStatusLabel: UILabel!
    let beaconManager = ESTBeaconManager()
     let beaconId = "73fd24d958354919574f799f6f7c0823"
     var centralManager:CBCentralManager?
    
     var timerA = Timer()
     var timerB = Timer()
     var timerAIsOn = false              // reset flag A
     var timerBIsOn = false              // reset flag B  
     var isBluetoothOn = false
    var isLocationOn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        centralManager = CBCentralManager(delegate: self, queue: nil)
        setupBeaconMonitoring()
        enableLocationServices()
        setTimer()
        revealViewController().delegate = self
    }
    @IBAction func onSettings(_ sender: Any) {
        self.revealViewController().revealToggle(animated: true)
    }
    @IBAction func onBeacon(_ sender: Any) {
        self.revealViewController().rightRevealToggle(animated: true)
    }
    func revealController(_ revealController: SWRevealViewController!, didMoveTo position: FrontViewPosition) {
        if position == .right{
            isSettingsView = true
        }
        else if isSettingsView {
            isSettingsView = false
            setTimer()
        }
    }
    func checkHadiStatus(){
        if isLocationOn && isBluetoothOn{
            hadiStatusLabel.text = "Hadi is active"
        }else {
            hadiStatusLabel.text = "Hadi is not active"
        }
    }
}


