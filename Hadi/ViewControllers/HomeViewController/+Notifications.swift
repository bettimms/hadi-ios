//
//  ViewController+Notifications.swift
//  HADI and MobileSilencer
//
//  Created by Betim S on 12/27/17.
//  Copyright © 2018 Spectratronix FZCO. All rights reserved.
//


import UserNotifications

extension HomeViewController{
    func showNotification(title: String, body: String) {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default()
        
        let request = UNNotificationRequest(
            identifier: "BeaconNotification", content: content, trigger: nil)
        let center = UNUserNotificationCenter.current()
        center.add(request, withCompletionHandler: nil)
    }
}
