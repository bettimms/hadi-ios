//
//  SelectAddressViewController+Locationmanager.swift
//  HADI and MobileSilencer
//
//  Created by Betim S on 12/31/17.
//  Copyright © 2017 Spectratronix. All rights reserved.
//

import UIKit

extension HomeViewController:CLLocationManagerDelegate{
    
    //TODO check if it's still in use
    func checkForLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            self.locationStatusLabel.text = "Location is ON"
//            self.enableBluetoothView.alpha = 0
//            UIView.animate(withDuration: 1.5, animations: {
//                self.enableLocationView.alpha = 1
//            })
            
        } else {
            self.locationStatusLabel.text = "Location is OFF"
//            UIView.animate(withDuration: 1.5, animations: {
//                self.enableLocationView.alpha = 0
//            })
        }
    }
    
    func enableLocationServices() {
        locationManager.delegate = self
        locationManager.distanceFilter = 0.05
        checkForLocationServices()
        
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            locationManager.requestWhenInUseAuthorization()
            break
        default:
            break
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
//        checkForLocationServices()
        switch status {
        case .notDetermined:
            // Request when-in-use authorization initially
            locationManager.requestWhenInUseAuthorization()
            break
        case .denied:
            self.isLocationOn = false
            locationStatusLabel.alpha = 1
            self.locationStatusLabel.text = "Location is OFF"
            break
        case .authorizedAlways, .authorizedWhenInUse:
            self.isLocationOn = true
            locationStatusLabel.alpha = 0
            self.locationStatusLabel.text = "Location is ON"
        default:
            break
        }
        checkHadiStatus()
    }
}
