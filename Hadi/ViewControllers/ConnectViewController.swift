//
//  ConnectViewController.swift
//  HADI and MobileSilencer
//
//  Created by Betim S on 12/2/17.
//  Copyright © 2018 Spectratronix FZCO. All rights reserved.
//

import UIKit

class ConnectViewController: UIViewController {
    
    public func hideViewController() -> Void {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func dismissConnectModal(_ sender: Any) {
        hideViewController()
    }
}
