//
//  PagingViewController.swift
//  HADI and MobileSilencer
//
//  Created by Betim S on 12/24/17.
//  Copyright © 2018 Spectratronix FZCO. All rights reserved.
//

import UIKit
import SWRevealViewController

class MainNavigationViewController: UINavigationController,SWRevealViewControllerDelegate {
    
    var isSettingsView:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let leftVC  = storyboard.instantiateViewController(withIdentifier: "SettingsVC")
        let rightVC  = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
        let orderBeaconVC = storyboard.instantiateViewController(withIdentifier: "OrderBeaconVC")
        let mainRevealController = SWRevealViewController(rearViewController: leftVC, frontViewController: rightVC)
        mainRevealController?.setRight(orderBeaconVC, animated: true)
        mainRevealController?.delegate = self
        self.addChildViewController(mainRevealController!)
        self.view.addGestureRecognizer((mainRevealController?.panGestureRecognizer())!)
        //       self.view.addGestureRecognizer((mainRevealController?.tapGestureRecognizer())!)
        
        mainRevealController?.rearViewRevealWidth = (self.view.frame.width)
        mainRevealController?.rightViewRevealWidth = (self.view.frame.width)
//        mainRevealController?.rearViewRevealDisplacement = 0
//        mainRevealController?.rightViewRevealDisplacement = 0
        mainRevealController?.frontViewShadowOpacity = 0
        mainRevealController?.clipsViewsToBounds = true
        mainRevealController?.rearViewRevealOverdraw = 0
        mainRevealController?.rightViewRevealOverdraw  = 0
    }
    
}

