//
//  SelectAddressViewController.swift
//  HADI and MobileSilencer
//
//  Created by Betim S on 12/26/17.
//  Copyright © 2017 Spectratronix. All rights reserved.
//

import UIKit
import GooglePlaces
import GooglePlacePicker
import FontAwesome_swift
import JVFloatLabeledTextField

class AddressViewController: UITableViewController {
    
    let locationManager = CLLocationManager()
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextView!
    @IBOutlet weak var pickMapButton: UIButton!
    @IBOutlet weak var phoneNumberTextField: JVFloatLabeledTextField!
    @IBOutlet weak var emailTextField: JVFloatLabeledTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.revealViewController().isEditing = true
        setbackBarButton()
        setMapButtonIcon()
        
    }
    func setMapButtonIcon(){
        pickMapButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 30)
        pickMapButton.setTitle(String.fontAwesomeIcon(name: .mapMarker), for: .normal)
    }
    func setbackBarButton(){
        let attributes = [NSAttributedStringKey.font:UIFont.fontAwesome(ofSize: 25)]
        let backItem = UIBarButtonItem(title: String.fontAwesomeIcon(name: .chevronLeft), style: .done, target: self, action:  #selector(goBack))
        backItem.setTitleTextAttributes(attributes, for: .normal)
        backItem.setTitleTextAttributes(attributes, for: .selected)
        backItem.tintColor = UIColor.white
        self.navigationItem.setLeftBarButton(backItem, animated: true)
    }
    @IBAction func goBack(_ sender: Any) {
        self.revealViewController().rightRevealToggle(animated: true)
    }
    
    @IBAction func pickPlace(_ sender: UIButton) {
        let center = CLLocationCoordinate2D(latitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePicker(config: config)
        
        placePicker.pickPlace(callback: {(place, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let place = place {
                var locationText = ""
                if let selectedPlace = place.formattedAddress{
                    let combinedComponents = selectedPlace.components(separatedBy: ", ").joined(separator: "\n")
                    locationText = "\(place.name)\n \(combinedComponents)\n\n Coordinates:\n Latitude: \(String(describing: place.coordinate.latitude))\n Longitude: \(String(describing: place.coordinate.longitude))"
                }
                else {
                    locationText = "\(place.name)"
                }
                self.addressTextField.text = locationText
                
            } else {
//                self.placeNameLabel.text = "No place selected"
                self.addressTextField.text = "No place selected"
            }
        })
    }

}
